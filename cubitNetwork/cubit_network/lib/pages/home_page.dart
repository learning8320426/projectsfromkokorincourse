import 'package:cubit_network/bloc/user_bloc.dart';
import 'package:cubit_network/bloc/user_event.dart';
import 'package:cubit_network/cubit/internet_cubit.dart';
import 'package:cubit_network/services/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../widgets/action_buttons.dart';
import '../widgets/user_list.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  // final userRepository = UserRepository();

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => UserRepository(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                UserBloc(userRepository: context.read<UserRepository>())
                  ..add(UserLoadEvent()),
          ),
          BlocProvider(create: (context) => ConnectionCubit()),
        ],
        child: Scaffold(
          appBar: AppBar(
            title: BlocConsumer<ConnectionCubit, MyConnectionState>(
              listener: (context, state) {
                if (state.connected == false) {
                  ScaffoldMessenger.of(context)
                      .showSnackBar(const SnackBar(content: Text('FUCK!!!')));
                }
                if (state.connected == true) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('ALL RIGHT!!!')));
                }
              },
              builder: (context, state) => state.connected
                  ? const Text('User List (в сети)')
                  : const Text(
                      'Нет соединения с интернет!',
                      style: TextStyle(color: Colors.red),
                    ),
            ),
            centerTitle: true,
          ),
          backgroundColor: Colors.white,
          body: const Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ActionButtons(),
              Expanded(child: UserList()),
            ],
          ),
        ),
      ),
    );
  }
}
